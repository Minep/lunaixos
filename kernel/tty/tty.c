#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "tty.h"
 
static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) 
{
	return fg | bg << 4;
}
 
static inline uint16_t vga_entry(unsigned char uc, uint8_t color) 
{
	return (uint16_t) uc | (uint16_t) color << 8;
}

 
static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
 
size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;
 
void tty_initialize(void) 
{
	terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
	terminal_buffer = (uint16_t*) 0xB8000;
	tty_clear();
}

void tty_settheme(enum vga_color foreground, enum vga_color background) {
    terminal_color = vga_entry_color(foreground, background);
}
 
void tty_setcolor(uint8_t color) 
{
	terminal_color = color;
}
 
void tty_putentryat(char c, uint8_t color, size_t x, size_t y) 
{
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = vga_entry(c, color);
}
 
void tty_putchar(char c) 
{
	if (c != '\n') {
        tty_putentryat(c, terminal_color, terminal_column, terminal_row);
        terminal_column++;
    }
    else {
        terminal_column = 0;
        terminal_row++;
    }
    
	if (terminal_column == VGA_WIDTH) {
		terminal_column = 0;
		if (terminal_row == VGA_HEIGHT)
			terminal_row = 0;
	}
}

void tty_clear(void) {
    for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_color);
		}
	}
	terminal_column = 0;
	terminal_row = 0;
}

void tty_scrollup(unsigned int row_offset) {
    if (row_offset == 0) return;

    size_t offset_size = row_offset * VGA_WIDTH;
    size_t remain = VGA_WIDTH * VGA_HEIGHT - offset_size;
    
    memcpy(terminal_buffer, terminal_buffer + offset_size, remain);
    for (size_t x = 0; x < VGA_WIDTH; x++) {
        for (size_t y = row_offset; y < VGA_HEIGHT; y++) {
            const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_color);
        }
    }
    
    
    if (terminal_row <= row_offset) {
        terminal_row = 0;
    }
    else {
        terminal_row-=row_offset;
    }
}

void tty_write(const char* data, size_t size) 
{
	for (size_t i = 0; i < size; i++)
		tty_putchar(data[i]);
}
 
void tty_writestring(const char* data) 
{
	tty_write(data, strlen(data));
}
 