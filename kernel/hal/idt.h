#ifndef __HAL_IDT
#define __HAL_IDT 1

#include <stdint.h>

#define IDT_SEG_PRESENT(x)    ((x) << 15)
#define IDT_DPL(x)            ((x) << 13)
#define IDT_GATE_32BITS(x)    ((x) << 11)

#define IDT_TASK        0x500
#define IDT_INT         0x600
#define IDT_TRAP        0x700

#define IDT_TASK_ENTRY(FLAGS)   (FLAGS) | IDT_TASK | IDT_GATE_32BITS(1)
#define IDT_INT_ENTRY(FLAGS)    (FLAGS) | IDT_INT | IDT_GATE_32BITS(1)
#define IDT_TRAP_ENTRY(FLAGS)   (FLAGS) | IDT_TRAP | IDT_GATE_32BITS(1)

#define IDT_INT_ENTRY_R0        IDT_INT_ENTRY(IDT_DPL(0) | IDT_SEG_PRESENT(1))
#define IDT_TASK_ENTRY_R0        IDT_TASK_ENTRY(IDT_DPL(0) | IDT_SEG_PRESENT(1))
#define IDT_TRAP_ENTRY_R0        IDT_TRAP_ENTRY(IDT_DPL(0) | IDT_SEG_PRESENT(1))


struct idt_loc {
    uint16_t limit;
    uint64_t* base;
};

void install_idt(void);

#endif