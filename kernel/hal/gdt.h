#include <stdint.h>

// see Intel 64 and IA-32 Arch. Software Development Manual Vol.3A  3.4.5

#define SEG_CODE_DATA(x)        ((x) << 4)     // Is code/data segement?
#define SEG_DPL(x)              ((x) << 5)     // Privilege level (Ring X)
#define SEG_PRESENT(x)           ((x) << 7)     // Is usable by system?
#define SEG_SYSAVL(x)           ((x) << 12)     // Is usable by system?
#define SEG_LONG(x)             ((x) << 13)     // Is Long mode segment?
#define SEG_32BITS(x)           ((x) << 14)     // For 32bits code?
#define SEG_4K_ALIGNED(x)       ((x) << 15)     // Is 4K aligned?

// from https://wiki.osdev.org/GDT_Tutorial
// see Intel 64 and IA-32 Arch. Software Development Manual Vol.3A  3.4.5.1

#define SEG_DATA_RD        0x00 // Read-Only
#define SEG_DATA_RDA       0x01 // Read-Only, accessed
#define SEG_DATA_RDWR      0x02 // Read/Write
#define SEG_DATA_RDWRA     0x03 // Read/Write, accessed
#define SEG_DATA_RDEXPD    0x04 // Read-Only, expand-down
#define SEG_DATA_RDEXPDA   0x05 // Read-Only, expand-down, accessed
#define SEG_DATA_RDWREXPD  0x06 // Read/Write, expand-down
#define SEG_DATA_RDWREXPDA 0x07 // Read/Write, expand-down, accessed
#define SEG_CODE_EX        0x08 // Execute-Only
#define SEG_CODE_EXA       0x09 // Execute-Only, accessed
#define SEG_CODE_EXRD      0x0A // Execute/Read
#define SEG_CODE_EXRDA     0x0B // Execute/Read, accessed
#define SEG_CODE_EXC       0x0C // Execute-Only, conforming
#define SEG_CODE_EXCA      0x0D // Execute-Only, conforming, accessed
#define SEG_CODE_EXRDC     0x0E // Execute/Read, conforming
#define SEG_CODE_EXRDCA    0x0F // Execute/Read, conforming, accessed

// defined for Ring 0 and Ring 3

#define SEG_CODE_R0     SEG_CODE_DATA(1) | SEG_DPL(0) | SEG_PRESENT(1) |\
                         SEG_SYSAVL(1) | SEG_LONG(0) | SEG_32BITS(1) |\
                         SEG_4K_ALIGNED(1) | SEG_CODE_EXRD

#define SEG_DATA_R0     SEG_CODE_DATA(1) | SEG_DPL(0) | SEG_PRESENT(1) |\
                         SEG_SYSAVL(1) | SEG_LONG(0) | SEG_32BITS(1) |\
                         SEG_4K_ALIGNED(1) | SEG_DATA_RDWR

#define SEG_CODE_R3     SEG_CODE_DATA(1) | SEG_DPL(3) | SEG_PRESENT(1) |\
                         SEG_SYSAVL(1) | SEG_LONG(0) | SEG_32BITS(1) |\
                         SEG_4K_ALIGNED(1) | SEG_CODE_EXRD
                         
#define SEG_DATA_R3     SEG_CODE_DATA(1) | SEG_DPL(3) | SEG_PRESENT(1) |\
                         SEG_SYSAVL(1) | SEG_LONG(0) | SEG_32BITS(1) |\
                         SEG_4K_ALIGNED(1) | SEG_DATA_RDWR

void install_gdt(void);