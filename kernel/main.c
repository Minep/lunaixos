#include "tty/tty.h"
#include "hal/gdt.h"
#include "hal/idt.h"
 
void kernel_init(void) {
    install_gdt();
    install_idt();
}

void kernel_entry(void) {
	tty_initialize();
    tty_settheme(VGA_COLOR_CYAN, VGA_COLOR_BLACK);
	
	tty_writestring("Hello, kernel World!\nTHis is new line.\n");
    tty_scrollup(1);    //scroll up by 1 row
    tty_writestring("This should be at second row.");
}