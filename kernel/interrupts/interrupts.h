#ifndef __INTERRUPTS
#define __INTERRUPTS 1
#include <stdint.h>

// Ref: Intel Manuel Vol.3 Figure 6-1
#define FAULT_DIVISION_ERROR 0x0
#define FAULT_TRAP_DEBUG_EXCEPTION 0x1
#define INT_NMI 0x2
#define TRAP_BREAKPOINT 0x3
#define TRAP_OVERFLOW 0x4
#define FAULT_BOUND_EXCEED 0x5
#define FAULT_INVALID_OPCODE 0x6
#define FAULT_NO_MATH_PROCESSOR 0x7
#define ABORT_DOUBLE_FAULT 0x8
#define FAULT_RESERVED_0 0x9
#define FAULT_INVALID_TSS 0xa
#define FAULT_SEG_NOT_PRESENT 0xb
#define FAULT_STACK_SEG_FAULT 0xc
#define FAULT_GENERAL_PROTECTION 0xd
#define FAULT_PAGE_FAULT 0xe
#define FAULT_RESERVED_1 0xf
#define FAULT_X87_FAULT 0x10
#define FAULT_ALIGNMENT_CHECK 0x11
#define ABORT_MACHINE_CHECK 0x12
#define FAULT_SIMD_FP_EXCEPTION 0x13
#define FAULT_VIRTUALIZATION_EXCEPTION 0x14
#define FAULT_CONTROL_PROTECTION 0x15

#pragma pack(push, 1)
typedef struct {
    uint32_t int_vector;
    uint32_t errorn;
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t esp;
    uint32_t ss;
} interrupt_context;
#pragma pack(pop)


void _int0();
void _int1();
void _int2();
void _int3();
void _int4();
void _int5();
void _int6();
void _int7();
void _int8();
void _int9();
void _int10();
void _int11();
void _int12();
void _int13();
void _int14();
void _int15();
void _int16();
void _int17();
void _int18();
void _int19();
void _int20();
void _int21();

void interrupt_handler (const interrupt_context *ctx);


#endif