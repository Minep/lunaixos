#include "interrupts.h"
#include <tty/tty.h>

void handle_isr(const interrupt_context *ctx) {
    tty_clear();
    tty_writestring("!!PANIC!!");
}

void interrupt_handler (const interrupt_context *ctx) {
    if (ctx->int_vector <= 31) {
        handle_isr(ctx);
    }
    // do some process
}