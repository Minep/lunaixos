.macro _int vector
    .global _int\vector
    .type _int\vector, @function
    _int\vector:
        pushl $\vector
        jmp interrupt_wrapper
.endm

.altmacro
.set i,0
.rept 22
    _int %i
    .set i,i+1
.endr

interrupt_wrapper:

    // pass the pointer to interrupt_ctx
    pushl %esp
    call interrupt_handler
    
    // remove the saved %esp and int_no from the top of stack
    // so the top of stack is pointed at the valid interrupted procedure stack
    // see intel manuel Vol.3A section 6.12.1 Fig 6-4
    addl $0x8, %esp
    iret