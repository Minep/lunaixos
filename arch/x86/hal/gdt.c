#include <hal/gdt.h>

#define GDT_ENTRIES 4


uint64_t _gdt[GDT_ENTRIES + 1]; // plus 1 for null descriptor
uint32_t _gdt_size = sizeof(_gdt) - 1;

static inline void set_gdt_entry(uint8_t i, uint32_t base, uint32_t limit, uint16_t flags) {
    // see Intel 64 and IA-32 Arch. Software Development Manual Vol.3A 3.4.5 fig. 3-8
    // higher 32 bits
    _gdt[i] =  (base & 0xff000000) | 
                (flags << 8) |
                (limit & 0xf0000) |
                ((base & 0x00ff0000) >> 16);
    _gdt[i] <<= 32;

    // lower 32bits
    _gdt[i] |= ((base & 0x0000ffff) << 16) | (limit & 0x0000ffff);
}

void install_gdt(void) {
    set_gdt_entry(0, 0x0, 0x0, 0x0);                //0x0  - NULL

    // Using basic flat model and paging (will be enable later)
    // Kernel (Ring 0)
    set_gdt_entry(1, 0x0, 0xFFFFFFFF, SEG_DATA_R0);      //0x08   Kernel accessable data
    set_gdt_entry(2, 0x0, 0xFFFFFFFF, SEG_CODE_R0);      //0x10   Kernel Code
    
    // reserved for user land (Ring 3)
    set_gdt_entry(3, 0x0, 0xFFFFFFFF, SEG_CODE_R3);      //0x18
    set_gdt_entry(4, 0x0, 0xFFFFFFFF, SEG_DATA_R3);      //0x20
}