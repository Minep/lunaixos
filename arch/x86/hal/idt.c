#include <hal/idt.h>
#include <interrupts/interrupts.h>

#define IDT_SIZE 31

uint64_t _idt[IDT_SIZE];
uint32_t _idt_size = sizeof(_idt) - 1;

static inline void set_idt_entry(uint8_t vector, void (*handler)(void), uint16_t flag) {
    uintptr_t handler_ptr = (uintptr_t) handler;

    _idt[vector] = (handler_ptr & 0xffff0000) | flag;
    _idt[vector] <<= 32;
    _idt[vector] |= (0x10 << 16) | (handler_ptr & 0x0000ffff);
}

void install_idt(void) {
    set_idt_entry(FAULT_DIVISION_ERROR, _int0, IDT_INT_ENTRY_R0);
    set_idt_entry(FAULT_TRAP_DEBUG_EXCEPTION, _int1, IDT_INT_ENTRY_R0);
    set_idt_entry(INT_NMI, _int2, IDT_INT_ENTRY_R0);
    set_idt_entry(TRAP_BREAKPOINT, _int3, IDT_TRAP_ENTRY_R0);
    set_idt_entry(TRAP_OVERFLOW, _int4, IDT_TRAP_ENTRY_R0);
}